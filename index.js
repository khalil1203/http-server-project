const http = require('http');
const fs = require('fs');
const uuid = require('uuid');
const { PORT } = require('./config');

function requestHandler(req, res) {
    req.url = req.url.toLowerCase();
    let arr = req.url.split("/");
    let delay_Time = arr[arr.length - 1];
    let statusCode = arr[arr.length - 1];


    if (req.url == "/html" && arr.length < 3) {

        fs.readFile('./data/page.html', (err, data) => {
            if (err) {
                console.log("oops can't show the html page!!!!");
                return res.end("Error!!!!!");
            }
            else {
                return res.end(data);
            }
        });
    }
    else if (req.url == "/json" && arr.length < 3) {
        fs.readFile('./data/page2.json', (err, data) => {
            if (err) {
                console.log("oops can't show the html page!!!!");
                return res.end("Error!!!!!");
            }
            else {
                return res.end((data));
            }
        });
    }
    else if (req.url == "/uuid" && arr.length < 3) {
        let generate_uuid = uuid.v4()
        res.end(generate_uuid);
    }
    else if (req.url.startsWith('/status') && arr.length < 4) {
        if (!http.STATUS_CODES[statusCode] || statusCode == 100) {
            return res.end("Do not provide wrong status code!!!!")
        }
        else {
            res.statusCode = statusCode;
            res.end(`{
                statuscode : ${statusCode},
                message : ${http.STATUS_CODES[statusCode]}
            }`);

        }
    }
    else if (req.url.startsWith("/delay") && arr.length < 4 && delay_Time >= 0) {
        setTimeout(() => {
            res.end(`Hello folks , sorry for wasting your ${delay_Time} seconds!!!!!`);
        }, delay_Time * 1000);
    }
    else if (req.url == "/") {
        fs.readFile('./data/homepage.html', (err, data) => {
            if (err) {
                console.log("oops can't show the html page!!!!");
                return res.end("Error!!!!!");
            }
            else {
                return res.end(data);
            }
        });
    }
    else {
        fs.readFile('./data/home.html', (err, data) => {
            if (err) {
                console.log("oops can't show the html page!!!!");
                return res.end("Error!!!!!");
            }
            else {
                return res.end(data);
            }
        });
    }
}




const server = http.createServer(requestHandler);

server.listen(PORT, (err) => {
    if (err) {
        console.log("oops server is not showing up!!!!");
    }
    else {
        console.log("server is up and running on port : ", PORT);
    }
});

