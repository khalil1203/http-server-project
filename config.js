const env = require('dotenv');
const result = env.config();
if (result.error) {
    console.log(result.error);
}
else {
    const { parsed: envs } = result;
    console.log(envs);
    module.exports = envs;
}